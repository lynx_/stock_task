from kivy.app import App

from kivy.uix.button import Button
from kivy.uix.widget import Widget
from kivy.uix.textinput import TextInput
from kivy.uix.gridlayout import  GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.popup import Popup
import threading
import socket
from time import sleep
from datetime import datetime,timedelta
import pygame

popupWindow = ''
Message_window = ''
pygame.mixer.init()

connected = 0

class DialogBox(BoxLayout):

	def btn(self):
		global connected
		if connected == 0:
			pygame.mixer.music.load("message.mp3")
			show_popup()
			self.sock = socket.socket()
			with open('server_name.txt','r') as file:
				info = file.read()
				HOST = info.split(' ')[0]
				PORT = int(info.split(' ')[1])

			self.sock.connect((HOST,PORT))
			connected = 1
			threading.Thread(target=self.accept,args=[]).start()

	def accept(self):
		count = 1
		now = datetime.date(datetime.now())
		while True:
			pygame.mixer.music.load("message.mp3")
			file = self.sock.recv(256).decode('utf-8')
			if now - datetime.date(datetime.now()) >= timedelta(hours=5):
				delete_files()
				now = datetime.date(datetime.now())
			with open(file, '+ab') as cont:
				while True:
					data = self.sock.recv(4096)
					if b'\x00' in data:
						cont.write(data[:-1])
						break
					elif not data:
						break
					cont.write(data)
			count+=1
			if count == 4:
				count = 1
				show_message()
				sleep(600)







class P(BoxLayout):
	def remove(self):
		global popupWindow
		popupWindow.dismiss()
 
class Message(BoxLayout):
	def remove(self):
		global Message_window
		Message_window.dismiss()
 

def show_popup():
	show = P()
	global popupWindow
	popupWindow = Popup(title = "Connect", content = show, size_hint = (None,None), size = (400,400))

	popupWindow.open()

	pygame.mixer.music.play()
	
def show_message():
	show = Message()
	global Message_window
	Message_window = Popup(title = "Message", content = show, size_hint = (None,None), size = (400,400))

	Message_window.open()
	pygame.mixer.music.play()

def delete_files():
	try:
		path = os.path.join(os.path.abspath(os.getcwd()), 'result_1_2.csv')
		os.remove(path)
	except:
		pass
	try:
		path = os.path.join(os.path.abspath(os.getcwd()), 'result3.csv')
		os.remove(path)
	except:
		pass
	try:
		path = os.path.join(os.path.abspath(os.getcwd()), 'result4.csv')
		os.remove(path)
	except:
		pass
	try:
		path = os.path.join(os.path.abspath(os.getcwd()), 'result4_feets.csv')
		os.remove(path)
	except:
		pass

class ClientApp(App):
	def build(self):
		return DialogBox()

		


if __name__ == '__main__':
	ClientApp().run()